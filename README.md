# ToggleTether
The app to toggle the Wifi tethering functionality.

## License
ToggleTether is licensed under the [MIT license](LICENSE).
