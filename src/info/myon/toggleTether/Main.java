package info.myon.toggleTether;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.widget.Toast;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiManager;
import java.lang.reflect.Method;

public class Main extends Activity {
  private boolean getTetherStatus() {
    WifiManager wifi = (WifiManager) getSystemService(Context.WIFI_SERVICE);
    try {
      Method method = wifi.getClass().getMethod("isWifiApEnabled");
      if("true".equals(method.invoke(wifi).toString())) {
        return true;
      } else {
        return false;
      }
    } catch (Exception e) {
      e.printStackTrace();
      return false;
    }
  }

  private void setTetherStatus(boolean status) {
    WifiManager wifi = (WifiManager) getSystemService(Context.WIFI_SERVICE);
    // turn-off wifi
    wifi.setWifiEnabled(false);

    try {
      Method method = wifi.getClass().getMethod("setWifiApEnabled", WifiConfiguration.class, boolean.class);
      method.invoke(wifi, null, status);
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  @Override
  public void onResume() {
    super.onResume();
    if (getTetherStatus()) {
      Toast.makeText(this, "Turn off tethering...", Toast.LENGTH_LONG).show();
      setTetherStatus(false);
    } else {
      Toast.makeText(this, "Turn on tethering...", Toast.LENGTH_LONG).show();
      setTetherStatus(true);
    }
    this.finish();
  }
}
